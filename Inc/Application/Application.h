/*
 * Application.h
 *
 *  Created on: 6 gru 2016
 *      Author: Kamil
 */

#ifndef APPLICATION_APPLICATION_H_
#define APPLICATION_APPLICATION_H_

#include "../CommonTypes/CommonTypes.h"
#include "../Board/Board.h"

Status_T Application_Perform(Aquarium_T *pkAquarium);


#endif /* APPLICATION_APPLICATION_H_ */
