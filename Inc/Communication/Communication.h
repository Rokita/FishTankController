/*
 * Communication.h
 *
 *  Created on: 6 gru 2016
 *      Author: Kamil
 */

#ifndef COMMUNICATION_COMMUNICATION_H_
#define COMMUNICATION_COMMUNICATION_H_

#include "../CommonTypes/CommonTypes.h"

Status_T CommunicationTransmit(Aquarium_T *pkAquarium);

#endif /* COMMUNICATION_COMMUNICATION_H_ */
