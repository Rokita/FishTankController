/*
 * Menu.h
 *
 *  Created on: 6 gru 2016
 *      Author: Kamil
 */

#ifndef MENU_MENU_H_
#define MENU_MENU_H_

#include "../CommonTypes/CommonTypes.h"

Status_T Menu_Perform(Aquarium_T *pkAquarium, Menu_T *pkMenu);



#endif /* MENU_MENU_H_ */
