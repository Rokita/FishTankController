/*
 * MenuSetClock.h
 *
 *  Created on: 6 kwi 2017
 *      Author: Kamil
 */

#ifndef MENU_SET_CLOCK_H_
#define MENU_SET_CLOCK_H_

#include "../../CommonTypes/CommonTypes.h"

Status_T Menu_SetClock_Perform(Aquarium_T *pkAquarium, Menu_T *pkMenu);

#endif /* MENU_SET_CLOCK_H_ */
