/*
 * MenuTemp.h
 *
 *  Created on: 6 kwi 2017
 *      Author: Kamil
 */

#ifndef MENU_TEMP_H_
#define MENU_TEMP_H_

#include "../../CommonTypes/CommonTypes.h"

Status_T Menu_Temp_Perform(Aquarium_T *pkAquarium, Menu_T *pkMenu);

#endif /* MENU_TEMP_H_ */
