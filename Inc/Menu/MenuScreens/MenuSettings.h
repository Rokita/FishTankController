/*
 * MenuSettings.h
 *
 *  Created on: 6 kwi 2017
 *      Author: Kamil
 */

#ifndef MENU_SETTINGS_H_
#define MENU_SETTINGS_H_

#include "../../CommonTypes/CommonTypes.h"

Status_T Menu_Settings_Perform(Aquarium_T *pkAquarium, Menu_T *pkMenu);

#endif /* MENU_SETTINGS_H_ */
