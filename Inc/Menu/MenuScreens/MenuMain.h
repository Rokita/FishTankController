/*
 * MenuMain.h
 *
 *  Created on: 6 kwi 2017
 *      Author: Kamil
 */

#ifndef MENU_MAIN_H_
#define MENU_MAIN_H_

#include "../../CommonTypes/CommonTypes.h"

Status_T Menu_Main_Perform(Aquarium_T *pkAquarium, Menu_T *pkMenu);

#endif /* MENU_MAIN_H_ */
