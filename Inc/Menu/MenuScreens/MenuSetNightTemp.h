/*
 * MenuSetNightTemp.h
 *
 *  Created on: 6 kwi 2017
 *      Author: Kamil
 */

#ifndef MENU_SET_NIGHT_TEMP_H_
#define MENU_SET_NIGHT_TEMP_H_

#include "../../CommonTypes/CommonTypes.h"

Status_T Menu_SetNightTemp_Perform(Aquarium_T *pkAquarium, Menu_T *pkMenu);

#endif /* MENU_SET_NIGHT_TEMP_H_ */
