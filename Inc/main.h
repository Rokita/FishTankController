/**
  ******************************************************************************
  * File Name          : main.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define WaterTemp_Pin GPIO_PIN_1
#define WaterTemp_GPIO_Port GPIOA
#define OutsideTemp_Pin GPIO_PIN_2
#define OutsideTemp_GPIO_Port GPIOA
#define ButtonAdc_Pin GPIO_PIN_3
#define ButtonAdc_GPIO_Port GPIOA
#define Led1_Pin GPIO_PIN_4
#define Led1_GPIO_Port GPIOA
#define Led2_Pin GPIO_PIN_5
#define Led2_GPIO_Port GPIOA
#define Led3_Pin GPIO_PIN_6
#define Led3_GPIO_Port GPIOA
#define LedAlive_Pin GPIO_PIN_7
#define LedAlive_GPIO_Port GPIOA
#define CoilHeater_Pin GPIO_PIN_0
#define CoilHeater_GPIO_Port GPIOB
#define Filter_Pin GPIO_PIN_1
#define Filter_GPIO_Port GPIOB
#define TxRx_Pin GPIO_PIN_10
#define TxRx_GPIO_Port GPIOB
#define RxTx_Pin GPIO_PIN_11
#define RxTx_GPIO_Port GPIOB
#define LcdBL_Pin GPIO_PIN_12
#define LcdBL_GPIO_Port GPIOB
#define LcdDc_Pin GPIO_PIN_8
#define LcdDc_GPIO_Port GPIOA
#define LcdCe_Pin GPIO_PIN_9
#define LcdCe_GPIO_Port GPIOA
#define LcdRst_Pin GPIO_PIN_10
#define LcdRst_GPIO_Port GPIOA
#define PowerLed_Pin GPIO_PIN_4
#define PowerLed_GPIO_Port GPIOB
#define DcFans_Pin GPIO_PIN_7
#define DcFans_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
