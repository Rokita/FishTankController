/*
 * Board.h
 *
 *  Created on: 6 gru 2016
 *      Author: Kamil
 */

#ifndef BOARD_BOARD_H_
#define BOARD_BOARD_H_

#include "../CommonTypes/CommonTypes.h"

Button_T GetPressedButton(void);
float GetWaterTemperature(void);
float GetAmbientTemperature(void);

Status_T SetLed(DeviceState_T eLed1, DeviceState_T eLed2, DeviceState_T eLed3);
Status_T SetFan(DeviceState_T eFan1);
Status_T SetCoilHeater(DeviceState_T  eCoilHeater);
Status_T SetFilter(DeviceState_T eFilter);
Status_T SetPowerLedInPercent(uint8_t u8PwmInPercent);


#endif /* BOARD_BOARD_H_ */
