/*
 * LcdPCD8544_Nokia5110_Lib.h
 *
 *  Created on: 20 gru 2016
 *      Author: Kamil
 */

#ifndef LCDPCD8544_NOKIA5110_LIB_LCDPCD8544_NOKIA5110_LIB_H_
#define LCDPCD8544_NOKIA5110_LIB_LCDPCD8544_NOKIA5110_LIB_H_

#include "stm32f1xx_hal.h"

typedef enum PowerMode
   {
   PowerMode_ActiveChip,
   PowerMode_PowerDown,
   PowerMode_LastUnused
   } PowerMode_T;

typedef enum EntryMode
   {
   EntryMode_HorizontalAddresing,
   EntryMode_VerticalAddresing,
   EntryMode_LastUnused
   } EntryMode_T;

typedef enum InstructionType
   {
   InstructionType_Basic,
   InstructionType_Extended,
   InstructionType_LastUnused
   } InstructionType_T;

typedef enum DisplayMode
   {
   DisplayMode_Blank,
   DisplayMode_Normal,
   DisplayMode_AllSegmentsOn,
   DisplayMode_Inverse,
   DisplayMode_LastUnused
   } DisplayMode_T;

HAL_StatusTypeDef LcdInit(SPI_HandleTypeDef *hspi);
void Lcd_SelectResetLine(GPIO_TypeDef *GpioPort, uint16_t GpioPin);
void Lcd_SelectCeLine(GPIO_TypeDef *GpioPort, uint16_t GpioPin);
void Lcd_SelectDcLine(GPIO_TypeDef *GpioPort, uint16_t GpioPin);
void Lcd_SelectBlLine(GPIO_TypeDef *GpioPort, uint16_t GpioPin);
void LcdReset_On();
void LcdReset_Off();
void LcdBackLight_On();
void LcdBackLight_Off();
HAL_StatusTypeDef LcdSetPos(uint8_t u8Line, uint8_t u8Pos);
HAL_StatusTypeDef LcdSetPowerMode(PowerMode_T ePowerMode);
HAL_StatusTypeDef LcdSetEntryMode(EntryMode_T eEntryMode);
HAL_StatusTypeDef LcdSetInstructionType(InstructionType_T eInstructionType);
HAL_StatusTypeDef LcdSetDisplayMode(DisplayMode_T eDisplayMode);

HAL_StatusTypeDef LcdDisplayText(uint8_t *u8Buffer, uint8_t u8Size);
HAL_StatusTypeDef LcdDisplayBuffer(uint8_t *u8Buffer, uint8_t u8Size);
HAL_StatusTypeDef LcdClearLine(uint8_t u8Line);
HAL_StatusTypeDef LcdClearDisplay(void);


#endif /* LCDPCD8544_NOKIA5110_LIB_LCDPCD8544_NOKIA5110_LIB_H_ */
