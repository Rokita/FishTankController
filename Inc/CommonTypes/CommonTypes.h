/*
 * CommonTypes.h
 *
 *  Created on: 6 gru 2016
 *      Author: Kamil
 */

#ifndef COMMONTYPES_COMMONTYPES_H_
#define COMMONTYPES_COMMONTYPES_H_

#include "../Inc/LcdPCD8544_Nokia5110_Lib/LcdPCD8544_Nokia5110_Lib.h"
#include "stm32f1xx_hal.h"

#include "stm32f1xx_hal.h"
#include "CommonTypes_WS2812.h"

#include <stdint.h>

typedef enum
   {
   false,
   true
   } bool;

ADC_HandleTypeDef hadc1;
RTC_HandleTypeDef hrtc;
SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;
TIM_HandleTypeDef htim3;
UART_HandleTypeDef huart3;

#define POWER_SUPLY     3.3

#define MCP9700_TEMP_COEFFICIENT    0.01
#define MCP9700_TEMP_0_VOLTAGE      0.5
#define ADC_RESOLUTION              4096

#define WATER_TEMP_CHANNEL    ADC_CHANNEL_1
#define AMBIENT_TEMP_CHANNEL  ADC_CHANNEL_2
#define BUTTON_ADC_CHANNEL    ADC_CHANNEL_3
#define BUTTON_STEP_VALUE     (4096/10)

#define MAIN_LOOP_DELAY_MS 50 //ms
#define MAX_OF_UINT32 (0xFFFFFFFF)

typedef struct Date
   {
      uint8_t u8Day;
      uint8_t u8Month;
      uint16_t u16Year;
      uint8_t u8Hour;
      uint8_t u8Minutes;
   } Date_T;

//////////////////////////////////////
typedef enum Status
//////////////////////////////////////
   {
   Status_Ok,
   Status_Error,
   Status_Busy
   } Status_T;

//////////////////////////////////////
typedef enum DeviceState
//////////////////////////////////////
   {
   Device_Off,
   Device_On
   } DeviceState_T;

//////////////////////////////////////
typedef enum State
//////////////////////////////////////
   {
   State_Idle,
   State_Heating,
   State_Cooling
   } State_T;

//////////////////////////////////////
typedef struct StateMachine
//////////////////////////////////////
   {
      State_T eCurrentState;
      State_T eLastState;
   } StateMachine_T;

//////////////////////////////////////
typedef enum Button
//////////////////////////////////////
   {
   Button_None,
   Button_Menu,
   Button_Left,
   Button_Right,
   Button_Up,
   Button_Down,
   Button_Enter,
   Button_Cancel,
   Button_ManualTheme,
   Button_AutomaticTheme,
   Button_Error
   } Button_T;

typedef enum Mode
   {
   eMode_Day,
   eMode_Night
   } Mode_T;

//////////////////////////////////////
typedef struct FishTankParameters
//////////////////////////////////////
   {
      WS2812Leds_T kWS2812Leds;
      float fDayTempToKeep;
      float fNightTempToKeep;
      float fWaterTemp;
      float fAmbientTemp;
      uint16_t u16PowerLedBrightness; // 0-999
      Mode_T eOperatingMode;
      Button_T ePressedButton;
      DeviceState_T eFans;
      DeviceState_T eHeater;
      DeviceState_T eFilter;
   } FishTankParameters_T;

//////////////////////////////////////
typedef struct Aquarium
//////////////////////////////////////
   {
      StateMachine_T kStateMachine;
      FishTankParameters_T kParameters;
      Date_T MyDate;
   } Aquarium_T;

//////////////////////////////////////
typedef enum Menu
//////////////////////////////////////
   {
   Menu_Main,
   Menu_Settings,
   Menu_Temp,
   Menu_SetDate,
   Menu_SetClock,
   Menu_SetNightTemp,
   Menu_SetDayTemp
   } Menu_T;



   uint32_t CalculateElapsedTime(uint32_t u32MyUwTick);

#endif /* COMMONTYPES_COMMONTYPES_H_ */
