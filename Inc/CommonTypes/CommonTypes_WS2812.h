/*
 * CommonTypes_WS2812.h
 *
 *  Created on: 26 maj 2017
 *      Author: Kamil
 */
#include <stdint.h>
#include "stm32f1xx_hal.h"
#include "CommonTypes_WS2812_Colors.h"

#ifndef COMMONTYPES_COMMONTYPES_WS2812_H_
#define COMMONTYPES_COMMONTYPES_WS2812_H_

typedef enum WS2812Mode
   {
   eWsMode_Automatic,
   eWsMode_Manual
   } WS2812Mode_T;

typedef struct WS2812Leds
   {
      WS2812Mode_T eMode;
      WS2812Theme_T eTheme;
   } WS2812Leds_T;

#endif /* COMMONTYPES_COMMONTYPES_WS2812_H_ */
