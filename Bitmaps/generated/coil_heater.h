#ifndef COIL_HEATER_H
#define COIL_HEATER_H

#define HEIGHT 8
#define WIDTH 28

const unsigned char coil_heater[] =
    {
	    0xFF,
	    0x91,
	    0x89,
	    0x85,
	    0xB5,
	    0xC9,
	    0xB5,
	    0x85,
	    0xB5,
	    0xC9,
	    0xB5,
	    0x85,
	    0xB5,
	    0xC9,
	    0xB5,
	    0x85,
	    0x89,
	    0x91,
	    0x91,
	    0xFF,
	    0x10,
	    0x10,
	    0x28,
	    0x44,
	    0x44,
	    0x7C,
	    0x28,
	    0x28,
    };

#endif
