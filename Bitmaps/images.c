#include "images.h"
#include "inc/coil_heater_image.h"
#include "inc/fan_image.h"
#include "inc/fish_image.h"
#include "inc/check_box_marked_image.h"
#include "inc/check_box_unmarked_image.h"

ImageStruct_T getImage(ImageID index)
   {
   ImageStruct_T image;
   switch (index)
      {
      case eCoilHeater:
         image = create_CoilHeaterImage();
         break;
      case eFan:
         image = create_FanImage();
         break;
      case eFish:
         image = create_FishImage();
         break;
      case eCheckBoxMarked:
         image = create_CheckBoxMarkedImage();
         break;
      case eCheckBoxUnmarked:
         image = create_CheckBoxUnmarkedImage();
         break;
      }
   image.pos.x = 0;
   image.pos.y = 0;

   return image;
   }
