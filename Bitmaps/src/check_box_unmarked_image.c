#include "../inc/check_box_unmarked_image.h"
#include "../generated/check_box_unmarked.h"

ImageStruct_T create_CheckBoxUnmarkedImage()
    {
    ImageStruct_T CheckBoxUnmarkedImage;
    CheckBoxUnmarkedImage.size.height = HEIGHT;
    CheckBoxUnmarkedImage.size.width = WIDTH;
    CheckBoxUnmarkedImage.image = (uint8_t *) check_box_unmarked;

    return CheckBoxUnmarkedImage;
    }

