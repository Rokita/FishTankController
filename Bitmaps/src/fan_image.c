#include "../inc/fan_image.h"
#include "../generated/fan.h"

ImageStruct_T create_FanImage()
    {
    ImageStruct_T FanImage;
    FanImage.size.height = HEIGHT;
    FanImage.size.width = WIDTH;
    FanImage.image = (uint8_t *) fan;

    return FanImage;
    }

