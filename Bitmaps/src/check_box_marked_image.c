#include "../inc/check_box_marked_image.h"
#include "../generated/check_box_marked.h"

ImageStruct_T create_CheckBoxMarkedImage()
    {
    ImageStruct_T CheckBoxMarkedImage;
    CheckBoxMarkedImage.size.height = HEIGHT;
    CheckBoxMarkedImage.size.width = WIDTH;
    CheckBoxMarkedImage.image = (uint8_t *) check_box_marked;

    return CheckBoxMarkedImage;
    }

