#include "../inc/fish_image.h"
#include "../generated/fish.h"

ImageStruct_T create_FishImage()
    {
    ImageStruct_T FishImage;
    FishImage.size.height = HEIGHT;
    FishImage.size.width = WIDTH;
    FishImage.image = (uint8_t *) fish;

    return FishImage;
    }
