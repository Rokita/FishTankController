#include "../inc/coil_heater_image.h"
#include "../generated/coil_heater.h"

ImageStruct_T create_CoilHeaterImage()
    {
    ImageStruct_T CoilHeaterImage;
    CoilHeaterImage.size.height = HEIGHT;
    CoilHeaterImage.size.width = WIDTH;
    CoilHeaterImage.image = (uint8_t *) coil_heater;

    return CoilHeaterImage;
    }

