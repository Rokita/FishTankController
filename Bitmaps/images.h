/*
 * images.h
 *
 *  Created on: 6 kwi 2017
 *      Author: Kamil
 */

#ifndef IMAGES_H_
#define IMAGES_H_

#include "inc/ImageStruct.h"

typedef enum imageId
    {
    eCoilHeater,
    eFan,
    eFish,
    eCheckBoxMarked,
    eCheckBoxUnmarked,
    eImages_numOf
    } ImageID;

ImageStruct_T getImage(ImageID inxed);

#endif /* IMAGES_H_ */
