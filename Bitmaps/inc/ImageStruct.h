#ifndef IMAGE_STRUCT_H
#define IMAGE_STRUCT_H

#include<stdint.h>
#include <stdlib.h>

typedef struct
{
	uint32_t width;
	uint32_t height;
}Size_T;

typedef struct
{
	uint32_t x;
	uint32_t y;
}Position_T;

typedef struct
{
	Position_T pos;
	Size_T size;
	uint8_t *image;
}ImageStruct_T;

#endif
