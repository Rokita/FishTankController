/**
 ******************************************************************************
 * File Name          : main.c
 * Description        : Main program body
 ******************************************************************************
 *
 * COPYRIGHT(c) 2017 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"

/* USER CODE BEGIN Includes */
#include "../Inc/CommonTypes/CommonTypes.h"
#include "../Inc/Application/Application.h"
#include "../Inc/Menu/Menu.h"
#include "../Inc/LcdPCD8544_Nokia5110_Lib/LcdPCD8544_Nokia5110_Lib.h"

#if 0
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

RTC_HandleTypeDef hrtc;

SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;

TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart3;

/* USER CODE BEGIN PV */
#endif
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_SPI2_Init(void);
static void MX_RTC_Init(void);
static void MX_TIM3_Init(void);
static void MX_SPI1_Init(void);

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void Config_LCD(void);
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
   {

   /* USER CODE BEGIN 1 */

   /* USER CODE END 1 */

   /* MCU Configuration----------------------------------------------------------*/

   /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
   HAL_Init();

   /* Configure the system clock */
   SystemClock_Config();

   /* Initialize all configured peripherals */
   MX_GPIO_Init();
   MX_ADC1_Init();
   MX_USART3_UART_Init();
   MX_SPI1_Init();
   MX_SPI2_Init();
   MX_TIM3_Init();
   MX_RTC_Init();

   /* USER CODE BEGIN 2 */

   HAL_ADCEx_Calibration_Start(&hadc1);

   Config_LCD();

   /* Create objects */
   Aquarium_T MyAquarium;
   Menu_T MyMenu;

   /* Initialize objects */
   MyAquarium.MyDate.u16Year = 2017;
   MyAquarium.MyDate.u8Month = 5;
   MyAquarium.MyDate.u8Day = 4;
   MyAquarium.MyDate.u8Hour = 12;
   MyAquarium.MyDate.u8Minutes = 15;

   MyAquarium.kStateMachine.eCurrentState = State_Idle;
   MyAquarium.kParameters.fDayTempToKeep = 20;
   MyAquarium.kParameters.fNightTempToKeep = 18;
   MyAquarium.kParameters.fAmbientTemp = 21;
   MyAquarium.kParameters.fWaterTemp = 19;
   MyAquarium.kParameters.u16PowerLedBrightness = 0;
   MyAquarium.kParameters.eFans = Device_Off;
   MyAquarium.kParameters.eFilter = Device_On;
   MyAquarium.kParameters.eHeater = Device_On;
   MyAquarium.kParameters.ePressedButton = Button_None;
   MyAquarium.kParameters.eOperatingMode = eMode_Day;

   MyAquarium.kParameters.kWS2812Leds.eMode = eWsMode_Automatic;
   MyAquarium.kParameters.kWS2812Leds.eTheme = eTheme_White;

   MyMenu = Menu_Main;
   HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
   /* USER CODE END 2 */

   /* Infinite loop */
   /* USER CODE BEGIN WHILE */
   while (1)
      {
      HAL_GPIO_TogglePin(LedAlive_GPIO_Port, LedAlive_Pin); // blinking led alive diode

      HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);

      MyAquarium.kParameters.ePressedButton = GetPressedButton();
      MyAquarium.kParameters.fWaterTemp = GetWaterTemperature();
      MyAquarium.kParameters.fAmbientTemp = GetAmbientTemperature();

      Menu_Perform(&MyAquarium, &MyMenu);
      Application_Perform(&MyAquarium);

      HAL_Delay(MAIN_LOOP_DELAY_MS);
      /* USER CODE END WHILE */

      /* USER CODE BEGIN 3 */
      }

   for (;;)
      {
      // software should never be here
      }
   /* USER CODE END 3 */

   }

/** System Clock Configuration
 */
void SystemClock_Config(void)
   {

   RCC_OscInitTypeDef RCC_OscInitStruct;
   RCC_ClkInitTypeDef RCC_ClkInitStruct;
   RCC_PeriphCLKInitTypeDef PeriphClkInit;

   /**Initializes the CPU, AHB and APB busses clocks
    */
   RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI
         | RCC_OSCILLATORTYPE_LSE;
   RCC_OscInitStruct.LSEState = RCC_LSE_ON;
   RCC_OscInitStruct.HSIState = RCC_HSI_ON;
   RCC_OscInitStruct.HSICalibrationValue = 16;
   RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
   RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
   RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
   if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
      {
      Error_Handler();
      }

   /**Initializes the CPU, AHB and APB busses clocks
    */
   RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
         | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
   RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
   RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
   RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
   RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

   if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
      {
      Error_Handler();
      }

   PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_ADC;
   PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
   PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV8;
   if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
      {
      Error_Handler();
      }

   /**Configure the Systick interrupt time
    */
   HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

   /**Configure the Systick
    */
   HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

   /* SysTick_IRQn interrupt configuration */
   HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
   }

/* ADC1 init function */
static void MX_ADC1_Init(void)
   {

   ADC_ChannelConfTypeDef sConfig;

   /**Common config
    */
   hadc1.Instance = ADC1;
   hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
   hadc1.Init.ContinuousConvMode = DISABLE;
   hadc1.Init.DiscontinuousConvMode = DISABLE;
   hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
   hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
   hadc1.Init.NbrOfConversion = 1;
   if (HAL_ADC_Init(&hadc1) != HAL_OK)
      {
      Error_Handler();
      }

   /**Configure Regular Channel
    */
   sConfig.Channel = ADC_CHANNEL_3;
   sConfig.Rank = 1;
   sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
   if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
      {
      Error_Handler();
      }

   }

/* RTC init function */
static void MX_RTC_Init(void)
   {

   RTC_TimeTypeDef sTime;
   RTC_DateTypeDef DateToUpdate;

   /**Initialize RTC Only
    */
   hrtc.Instance = RTC;
   hrtc.Init.AsynchPrediv = RTC_AUTO_1_SECOND;
   hrtc.Init.OutPut = RTC_OUTPUTSOURCE_NONE;
   if (HAL_RTC_Init(&hrtc) != HAL_OK)
      {
      Error_Handler();
      }

   /**Initialize RTC and set the Time and Date
    */
   if (HAL_RTCEx_BKUPRead(&hrtc, RTC_BKP_DR1) != 0x32F2)
      {
      sTime.Hours = 0x1;
      sTime.Minutes = 0x0;
      sTime.Seconds = 0x0;

      if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
         {
         Error_Handler();
         }

      DateToUpdate.WeekDay = RTC_WEEKDAY_MONDAY;
      DateToUpdate.Month = RTC_MONTH_JANUARY;
      DateToUpdate.Date = 0x1;
      DateToUpdate.Year = 0x0;

      if (HAL_RTC_SetDate(&hrtc, &DateToUpdate, RTC_FORMAT_BCD) != HAL_OK)
         {
         Error_Handler();
         }

      HAL_RTCEx_BKUPWrite(&hrtc, RTC_BKP_DR1, 0x32F2);
      }

   }

/* SPI1 init function */
static void MX_SPI1_Init(void)
   {

   hspi1.Instance = SPI1;
   hspi1.Init.Mode = SPI_MODE_MASTER;
   hspi1.Init.Direction = SPI_DIRECTION_2LINES;
   hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
   hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
   hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
   hspi1.Init.NSS = SPI_NSS_SOFT;
   hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
   hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
   hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
   hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
   hspi1.Init.CRCPolynomial = 10;
   if (HAL_SPI_Init(&hspi1) != HAL_OK)
      {
      Error_Handler();
      }

   }

/* SPI2 init function */
static void MX_SPI2_Init(void)
   {

   hspi2.Instance = SPI2;
   hspi2.Init.Mode = SPI_MODE_MASTER;
   hspi2.Init.Direction = SPI_DIRECTION_2LINES;
   hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
   hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
   hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
   hspi2.Init.NSS = SPI_NSS_SOFT;
   hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
   hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
   hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
   hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
   hspi2.Init.CRCPolynomial = 10;
   if (HAL_SPI_Init(&hspi2) != HAL_OK)
      {
      Error_Handler();
      }

   }

/* TIM3 init function */
static void MX_TIM3_Init(void)
   {

   TIM_MasterConfigTypeDef sMasterConfig;
   TIM_OC_InitTypeDef sConfigOC;

   htim3.Instance = TIM3;
   htim3.Init.Prescaler = 239;
   htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
   htim3.Init.Period = 999;
   htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
   if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
      {
      Error_Handler();
      }

   sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
   sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
   if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
      {
      Error_Handler();
      }

   sConfigOC.OCMode = TIM_OCMODE_PWM1;
   sConfigOC.Pulse = 0;
   sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
   sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
   if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
      {
      Error_Handler();
      }

   HAL_TIM_MspPostInit(&htim3);

   }

/* USART3 init function */
static void MX_USART3_UART_Init(void)
   {

   huart3.Instance = USART3;
   huart3.Init.BaudRate = 38400;
   huart3.Init.WordLength = UART_WORDLENGTH_8B;
   huart3.Init.StopBits = UART_STOPBITS_1;
   huart3.Init.Parity = UART_PARITY_NONE;
   huart3.Init.Mode = UART_MODE_TX_RX;
   huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
   huart3.Init.OverSampling = UART_OVERSAMPLING_16;
   if (HAL_UART_Init(&huart3) != HAL_OK)
      {
      Error_Handler();
      }

   }

/** Configure pins as 
 * Analog
 * Input
 * Output
 * EVENT_OUT
 * EXTI
 */
static void MX_GPIO_Init(void)
   {

   GPIO_InitTypeDef GPIO_InitStruct;

   /* GPIO Ports Clock Enable */
   __HAL_RCC_GPIOC_CLK_ENABLE()
   ;
   __HAL_RCC_GPIOA_CLK_ENABLE()
   ;
   __HAL_RCC_GPIOB_CLK_ENABLE()
   ;

   /*Configure GPIO pin Output Level */
   HAL_GPIO_WritePin(GPIOA,
         Led1_Pin | Led2_Pin | Led3_Pin | LedAlive_Pin | LcdDc_Pin | LcdCe_Pin
               | LcdRst_Pin, GPIO_PIN_RESET);

   /*Configure GPIO pin Output Level */
   HAL_GPIO_WritePin(GPIOB,
   CoilHeater_Pin | Filter_Pin | LcdBL_Pin | DcFans_Pin, GPIO_PIN_RESET);

   /*Configure GPIO pins : Led1_Pin Led2_Pin Led3_Pin LedAlive_Pin
    LcdDc_Pin LcdCe_Pin LcdRst_Pin */
   GPIO_InitStruct.Pin = Led1_Pin | Led2_Pin | Led3_Pin | LedAlive_Pin
         | LcdDc_Pin | LcdCe_Pin | LcdRst_Pin;
   GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
   GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
   HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

   /*Configure GPIO pins : CoilHeater_Pin Filter_Pin LcdBL_Pin DcFans_Pin */
   GPIO_InitStruct.Pin = CoilHeater_Pin | Filter_Pin | LcdBL_Pin | DcFans_Pin;
   GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
   GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
   HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

   }

/* USER CODE BEGIN 4 */
void Config_LCD(void)
   {
   Lcd_SelectBlLine(LcdBL_GPIO_Port, LcdBL_Pin);
   Lcd_SelectCeLine(LcdCe_GPIO_Port, LcdCe_Pin);
   Lcd_SelectDcLine(LcdDc_GPIO_Port, LcdDc_Pin);
   Lcd_SelectResetLine(LcdRst_GPIO_Port, LcdRst_Pin);
   //LcdSelectResetLine must be executed before LcdInit - Kamil Description
   LcdInit(&hspi2);
   //10ms delay is required for initialize LCD - Kamil Description
   HAL_Delay(10);
   LcdReset_Off();
   LcdBackLight_On();
   LcdSetPos(0, 0);
   LcdSetDisplayMode(DisplayMode_Normal);
   LcdSetEntryMode(EntryMode_HorizontalAddresing);
   LcdSetInstructionType(InstructionType_Basic);
   LcdSetPowerMode(PowerMode_ActiveChip);
   LcdClearDisplay();
   }

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
void Error_Handler(void)
   {
   /* USER CODE BEGIN Error_Handler */
   /* User can add his own implementation to report the HAL error return state */
   while (1)
      {
      }
   /* USER CODE END Error_Handler */
   }

#ifdef USE_FULL_ASSERT

/**
 * @brief Reports the name of the source file and the source line number
 * where the assert_param error has occurred.
 * @param file: pointer to the source file name
 * @param line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
   {
   /* USER CODE BEGIN 6 */
   /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
   /* USER CODE END 6 */

   }

#endif

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
