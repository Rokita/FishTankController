#include "../../Inc/CommonTypes/CommonTypes.h"

uint32_t CalculateElapsedTime(uint32_t u32MyUwTick)
   {
   uint32_t u32GetTick = HAL_GetTick();
   if (u32MyUwTick > u32GetTick) //if variable uwTick overload
      {
      return ( MAX_OF_UINT32 - u32MyUwTick + u32GetTick);
      }
   else
      {
      return (u32GetTick - u32MyUwTick);
      }
   }
