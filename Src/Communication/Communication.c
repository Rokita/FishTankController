/*
 * Communication.c
 *
 *  Created on: 6 gru 2016
 *      Author: Kamil
 */

#include "../../Inc/Communication/Communication.h"

/*********************************************************
 *********         PRIVATE FUNCTIONS              ********
 ********************************************************/
uint16_t PackAquariumData(Aquarium_T *pkAquarium, uint8_t *u8Data)
   {
   return sprintf(u8Data, "Button : %d \t WaterTemp : %f \t AmbientTemp : %f \n\r", (uint8_t) pkAquarium->kParameters.ePressedButton, pkAquarium->kParameters.fWaterTemp, pkAquarium->kParameters.fAmbientTemp);
   }
/*********************************************************
 *********          PUBLIC FUNCTIONS              ********
 ********************************************************/

//////////////////////////////////////
Status_T CommunicationTransmit(Aquarium_T *pkAquarium)
//////////////////////////////////////
   {
   uint8_t u8Data[100];
   uint8_t u8Size = PackAquariumData(pkAquarium, u8Data);
   HAL_UART_Transmit(&huart3, u8Data, u8Size, 100);
   return Status_Ok;
   }
