/*
 * Application.c
 *
 *  Created on: 6 gru 2016
 *      Author: Kamil
 */

#include "../../Inc/Application/Application.h"

#define TEMPERATURE_HYSTERESIS (1)

static uint32_t u32MyUwTick = 0;
/*********************************************************
 *********      PRIVATE FUNCTIONS DECLARATION     ********
 ********************************************************/
Status_T StateIdle_Perform(Aquarium_T *pkAquarium);
Status_T StateCooling_Perform(Aquarium_T *pkAquarium);
Status_T StateHeating_Perform(Aquarium_T *pkAquarium);

void HandleButton_ManualTheme(Aquarium_T *pkAquarium);
void HandleButton_AutomaticTheme(Aquarium_T *pkAquarium);
void AutomaticColorTheme_Perform(Aquarium_T *pkAquarium);
/*********************************************************
 *********      PUBLIC FUNCTIONS DEFINITION       ********
 ********************************************************/

void updateSubsystem(Aquarium_T *pkAquarium);

Status_T Application_Perform(Aquarium_T *pkAquarium)
   {
   if (pkAquarium->kParameters.ePressedButton == Button_ManualTheme)
      {
      HandleButton_ManualTheme(pkAquarium);
      }
   if (pkAquarium->kParameters.ePressedButton == Button_AutomaticTheme)
      {
      HandleButton_AutomaticTheme(pkAquarium);
      }
   if (pkAquarium->kParameters.kWS2812Leds.eMode == eWsMode_Automatic)
      {
      AutomaticColorTheme_Perform(pkAquarium);
      }

   float fTempToKeep = pkAquarium->kParameters.fDayTempToKeep;
   if (pkAquarium->kParameters.eOperatingMode == eMode_Night)
      {
      fTempToKeep = pkAquarium->kParameters.fNightTempToKeep;
      }

   if (pkAquarium->kParameters.fWaterTemp < (fTempToKeep - TEMPERATURE_HYSTERESIS))
      {
      pkAquarium->kStateMachine.eCurrentState = State_Heating;
      }
   else if (pkAquarium->kParameters.fWaterTemp > (fTempToKeep + TEMPERATURE_HYSTERESIS))
      {
      pkAquarium->kStateMachine.eCurrentState = State_Cooling;
      }
   else
      {
      pkAquarium->kStateMachine.eCurrentState = State_Idle;
      }

   Status_T eReturnStatus = Status_Error;

   switch (pkAquarium->kStateMachine.eCurrentState)
      {
      case State_Idle:
         eReturnStatus = StateIdle_Perform(pkAquarium);
         break;
      case State_Cooling:
         u32MyUwTick = HAL_GetTick();
         eReturnStatus = StateCooling_Perform(pkAquarium);
         break;
      case State_Heating:
         u32MyUwTick = HAL_GetTick();
         eReturnStatus = StateHeating_Perform(pkAquarium);
         break;
      default:   //This is required in case of wrong value in state machine.
         break;
      }
   updateSubsystem(pkAquarium);
   return eReturnStatus;
   }

void updateSubsystem(Aquarium_T *pkAquarium)
   {
   SetFan(pkAquarium->kParameters.eFans);
   SetCoilHeater(pkAquarium->kParameters.eHeater);
   SetFilter(pkAquarium->kParameters.eFilter);
   }

/*********************************************************
 *********      PRIVATE FUNCTIONS DEFINITION      ********
 ********************************************************/
//////////////9////////////////////////
Status_T StateIdle_Perform(Aquarium_T *pkAquarium)
//////////////////////////////////////
   {
   pkAquarium->kParameters.eFans = Device_Off;
   pkAquarium->kParameters.eHeater = Device_Off;
   if (CalculateElapsedTime(u32MyUwTick) > 3600000) //if Elapsed time > 1 hour
      {
      if (CalculateElapsedTime(u32MyUwTick) < 4200000) // if Elapsed Time < 1 hour 10 min
         {
         pkAquarium->kParameters.eFilter = Device_On;
         }
      else
         {
         u32MyUwTick = HAL_GetTick();
         pkAquarium->kParameters.eFilter = Device_Off;
         }
      }
   return Status_Ok;
   }

//////////////////////////////////////
Status_T StateCooling_Perform(Aquarium_T *pkAquarium)
//////////////////////////////////////
   {
   pkAquarium->kParameters.eFans = Device_On;
   pkAquarium->kParameters.eFilter = Device_On;
   pkAquarium->kParameters.eHeater = Device_Off;
   return Status_Ok;
   }

//////////////////////////////////////
Status_T StateHeating_Perform(Aquarium_T *pkAquarium)
//////////////////////////////////////
   {
   pkAquarium->kParameters.eFans = Device_Off;
   pkAquarium->kParameters.eFilter = Device_On;
   pkAquarium->kParameters.eHeater = Device_On;
   return Status_Ok;
   }

void HandleButton_ManualTheme(Aquarium_T *pkAquarium)
   {
   pkAquarium->kParameters.kWS2812Leds.eMode = eWsMode_Manual;
   WS2812Theme_T eSelectedTheme = pkAquarium->kParameters.kWS2812Leds.eTheme;
   eSelectedTheme = (pkAquarium->kParameters.kWS2812Leds.eTheme + 1) % eTheme_NumOf;
   pkAquarium->kParameters.kWS2812Leds.eTheme = eSelectedTheme;
   HAL_SPI_Transmit(&hspi1, WS2812_ColorsTab[eSelectedTheme],
   THEME_BUFFER_SIZE, 1000);
   }


void HandleButton_AutomaticTheme(Aquarium_T *pkAquarium)
   {
   pkAquarium->kParameters.kWS2812Leds.eMode = eWsMode_Automatic;
   }

void AutomaticColorTheme_Perform(Aquarium_T *pkAquarium)
   {
   WS2812Theme_T eActualTheme = eTheme_NavyBlue;
   if(pkAquarium->MyDate.u8Hour > 6)
      {
      eActualTheme = eTheme_White;
      }
   if(pkAquarium->MyDate.u8Hour > 7)
      {
      eActualTheme = eTheme_WhiteAndRed;
      }
   if(pkAquarium->MyDate.u8Hour > 9)
      {
      eActualTheme = eTheme_GreenAndYellow;
      }
   if(pkAquarium->MyDate.u8Hour > 12)
      {
      eActualTheme = eTheme_Blue;
      }
   if(pkAquarium->MyDate.u8Hour > 14)
      {
      eActualTheme = eTheme_Purple;
      }
   if(pkAquarium->MyDate.u8Hour > 17)
      {
      eActualTheme = eTheme_OrangeAndPink;
      }
   if(pkAquarium->MyDate.u8Hour > 19)
      {
      eActualTheme = eTheme_RedAndBlue;
      }
   if(pkAquarium->MyDate.u8Hour > 23)
      {
      eActualTheme = eTheme_NavyBlue;
      }
   pkAquarium->kParameters.kWS2812Leds.eTheme = eActualTheme;
   HAL_SPI_Transmit(&hspi1, WS2812_ColorsTab[eActualTheme], THEME_BUFFER_SIZE, 1000);

   }
