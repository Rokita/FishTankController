/*
 * LcdPCD8544_Nokia5110_Lib.c
 *
 *  Created on: 20 gru 2016
 *      Author: Kamil
 */

#include "../../Inc/LcdPCD8544_Nokia5110_Lib/LcdPCD8544_Nokia5110_Lib.h"
#include "../../Inc/LcdPCD8544_Nokia5110_Lib/LcdPCD8544_Ascii.h"

#define CHAR_SIZE    5
#define LCD_ROWS     6
#define LCD_COLUMNS  84
#define LCD_MAX_CHAR_SIZE  ((LCD_ROWS)*(LCD_COLUMNS))

#define CONTROL_TEMPERATURE_MASK 0x04
#define CONTROL_DISPLAY_MASK     0x08
#define CONTROL_BIAS_SYSTEM_MASK 0x10
#define CONTROL_FUNCTION_MASK    0x20
#define CONTROL_Y_LINE_MASK      0x40
#define CONTROL_X_POS_MASK       0x80

#define LCD_LINE_MIN 0
#define LCD_LINE_MAX 5
#define LCD_POS_MIN  0
#define LCD_POS_MAX  83

#define INSTRUCTION_TYPE_POS  0
#define ENTRY_MODE_POS        1
#define POWER_MODE_POS        2

uint8_t tu8DisplayMode[4] =
   {
   0x00, 0x04, 0x01, 0x05
   };

typedef struct LcdPcd8544Line
   {
      GPIO_TypeDef *GpioPort;
      uint16_t GpioPin;
      GPIO_PinState GpioState;
   } LcdPcd8544Line_T;
typedef struct LcdPcd8544Struct
   {
      SPI_HandleTypeDef *hspi;
      LcdPcd8544Line_T kLineReset;
      LcdPcd8544Line_T kLineCe;
      LcdPcd8544Line_T kLineDc;
      LcdPcd8544Line_T kLineBl;
      PowerMode_T ePowerMode;
      EntryMode_T eEntryMode;
      InstructionType_T eInstructionType;
      DisplayMode_T eDisplayMode;
   } LcdPcd8544Struct_T;

/***************************
 *    Global Lcd Struct
 **************************/
LcdPcd8544Struct_T kLcd;
/**************************/

#define ASSERT_SPI {\
 if(kLcd.hspi == NULL)\
    return HAL_ERROR;\
}
#define ASSERT_PARAMETER(condition) {\
 if((condition))\
    return HAL_ERROR;\
}

void ExecCommand(uint8_t u8Command)
   {
   kLcd.kLineDc.GpioState = RESET;
   HAL_GPIO_WritePin(kLcd.kLineDc.GpioPort, kLcd.kLineDc.GpioPin, kLcd.kLineDc.GpioState);
   kLcd.kLineCe.GpioState = RESET;
   HAL_GPIO_WritePin(kLcd.kLineCe.GpioPort, kLcd.kLineCe.GpioPin, kLcd.kLineCe.GpioState);

   HAL_SPI_Transmit(kLcd.hspi, &u8Command, 1, 100);

   kLcd.kLineCe.GpioState = SET;
   HAL_GPIO_WritePin(kLcd.kLineCe.GpioPort, kLcd.kLineCe.GpioPin, kLcd.kLineCe.GpioState);
   }
void ExecFunctionSet()
   {
   uint8_t u8Command = 0 | CONTROL_FUNCTION_MASK | (kLcd.ePowerMode << POWER_MODE_POS)
         | (kLcd.eEntryMode << ENTRY_MODE_POS) | (kLcd.eInstructionType << INSTRUCTION_TYPE_POS);
   ExecCommand(u8Command);
   }

HAL_StatusTypeDef LcdInit(SPI_HandleTypeDef *hspi)
   {
   ASSERT_PARAMETER(hspi==NULL)

   kLcd.hspi = hspi;
   kLcd.kLineReset.GpioState = GPIO_PIN_RESET;
   HAL_GPIO_WritePin(kLcd.kLineReset.GpioPort, kLcd.kLineReset.GpioPin, kLcd.kLineReset.GpioState);
   HAL_Delay(1);
   kLcd.kLineReset.GpioState = GPIO_PIN_SET;
   HAL_GPIO_WritePin(kLcd.kLineReset.GpioPort, kLcd.kLineReset.GpioPin, kLcd.kLineReset.GpioState);
   ExecCommand(0x21);
   ExecCommand(0x14);
   ExecCommand(0x80 | 0x2f);  //Ustawienie kontrastu
   ExecCommand(0x20);
   ExecCommand(0x0c);

   return HAL_OK;
   }
void Lcd_SelectResetLine(GPIO_TypeDef *GpioPort, uint16_t GpioPin)
   {
   kLcd.kLineReset.GpioPort = GpioPort;
   kLcd.kLineReset.GpioPin = GpioPin;
   kLcd.kLineReset.GpioState = RESET;
   }
void Lcd_SelectCeLine(GPIO_TypeDef *GpioPort, uint16_t GpioPin)
   {
   kLcd.kLineCe.GpioPort = GpioPort;
   kLcd.kLineCe.GpioPin = GpioPin;
   kLcd.kLineCe.GpioState = RESET;
   }
void Lcd_SelectDcLine(GPIO_TypeDef *GpioPort, uint16_t GpioPin)
   {
   kLcd.kLineDc.GpioPort = GpioPort;
   kLcd.kLineDc.GpioPin = GpioPin;
   kLcd.kLineDc.GpioState = RESET;
   }
void Lcd_SelectBlLine(GPIO_TypeDef *GpioPort, uint16_t GpioPin)
   {
   kLcd.kLineBl.GpioPort = GpioPort;
   kLcd.kLineBl.GpioPin = GpioPin;
   kLcd.kLineBl.GpioState = RESET;
   }
void LcdReset_On()
   {
   kLcd.kLineReset.GpioState = GPIO_PIN_RESET;
   HAL_GPIO_WritePin(kLcd.kLineReset.GpioPort, kLcd.kLineReset.GpioPin, kLcd.kLineReset.GpioState);
   }
void LcdReset_Off()
   {
   kLcd.kLineReset.GpioState = GPIO_PIN_SET;
   HAL_GPIO_WritePin(kLcd.kLineReset.GpioPort, kLcd.kLineReset.GpioPin, kLcd.kLineReset.GpioState);
   }
void LcdBackLight_On()
   {
   kLcd.kLineBl.GpioState = GPIO_PIN_SET;
   HAL_GPIO_WritePin(kLcd.kLineBl.GpioPort, kLcd.kLineBl.GpioPin, kLcd.kLineBl.GpioState);
   }
void LcdBackLight_Off()
   {
   kLcd.kLineBl.GpioState = GPIO_PIN_RESET;
   HAL_GPIO_WritePin(kLcd.kLineBl.GpioPort, kLcd.kLineBl.GpioPin, kLcd.kLineBl.GpioState);
   }
HAL_StatusTypeDef LcdSetPos(uint8_t u8Line, uint8_t u8Pos)
   {
   ASSERT_SPI
   ASSERT_PARAMETER(u8Line < LCD_LINE_MIN)
   ASSERT_PARAMETER(u8Line > LCD_LINE_MAX)
   ASSERT_PARAMETER(u8Pos < LCD_POS_MIN)
   ASSERT_PARAMETER(u8Pos > LCD_POS_MAX)
   ASSERT_PARAMETER(kLcd.kLineReset.GpioState == GPIO_PIN_RESET)

   uint8_t u8Command;
   u8Command = 0 | CONTROL_Y_LINE_MASK | u8Line;
   ExecCommand(u8Command);
   u8Command = 0 | CONTROL_X_POS_MASK | u8Pos;
   ExecCommand(u8Command);
   return HAL_OK;
   }
HAL_StatusTypeDef LcdSetPowerMode(PowerMode_T ePowerMode)
   {
   ASSERT_SPI
   ASSERT_PARAMETER(ePowerMode > PowerMode_LastUnused)

   kLcd.ePowerMode = ePowerMode;
   ExecFunctionSet();
   return HAL_OK;
   }
HAL_StatusTypeDef LcdSetEntryMode(EntryMode_T eEntryMode)
   {
   ASSERT_SPI
   ASSERT_PARAMETER(eEntryMode > EntryMode_LastUnused)

   kLcd.eEntryMode = eEntryMode;
   ExecFunctionSet();
   return HAL_OK;
   }
HAL_StatusTypeDef LcdSetInstructionType(InstructionType_T eInstructionType)
   {
   ASSERT_SPI
   ASSERT_PARAMETER(eInstructionType > InstructionType_LastUnused)

   kLcd.eInstructionType = eInstructionType;
   ExecFunctionSet();
   return HAL_OK;
   }
HAL_StatusTypeDef LcdSetDisplayMode(DisplayMode_T eDisplayMode)
   {
   ASSERT_SPI
   ASSERT_PARAMETER(eDisplayMode > DisplayMode_LastUnused)

   kLcd.eDisplayMode = eDisplayMode;
   uint8_t u8Command = 0 | CONTROL_DISPLAY_MASK | (tu8DisplayMode[kLcd.eDisplayMode]);
   ExecCommand(u8Command);
   return HAL_OK;
   }

HAL_StatusTypeDef LcdDisplayText(uint8_t *u8Buffer, uint8_t u8Size)
   {
   ASSERT_SPI
   ASSERT_PARAMETER(u8Buffer == NULL)
   ASSERT_PARAMETER(u8Size > LCD_MAX_CHAR_SIZE)

   uint8_t u8BufferBytes[CHAR_SIZE * LCD_MAX_CHAR_SIZE];
   uint8_t u8AsciiCharNumber;
   uint8_t u8ByteValue;
   for (uint8_t u8IterChar = 0; u8IterChar < u8Size; u8IterChar++)
      {
      for (uint8_t u8Byte = 0; u8Byte < CHAR_SIZE; u8Byte++)
         {
         u8AsciiCharNumber = u8Buffer[u8IterChar] - ASCII_OFFSET;
         u8ByteValue = ASCII[u8AsciiCharNumber][u8Byte];
         u8BufferBytes[u8IterChar * CHAR_SIZE + u8Byte] = u8ByteValue;
         }
      }
   kLcd.kLineDc.GpioState = SET;
   HAL_GPIO_WritePin(kLcd.kLineDc.GpioPort, kLcd.kLineDc.GpioPin, kLcd.kLineDc.GpioState);
   kLcd.kLineCe.GpioState = RESET;
   HAL_GPIO_WritePin(kLcd.kLineCe.GpioPort, kLcd.kLineCe.GpioPin, kLcd.kLineCe.GpioState);

   HAL_SPI_Transmit(kLcd.hspi, u8BufferBytes, u8Size * CHAR_SIZE, 1000);
   HAL_Delay(1);
   kLcd.kLineCe.GpioState = SET;
   HAL_GPIO_WritePin(kLcd.kLineCe.GpioPort, kLcd.kLineCe.GpioPin, kLcd.kLineCe.GpioState);
   return HAL_OK;
   }

HAL_StatusTypeDef LcdDisplayBuffer(uint8_t *u8Buffer, uint8_t u8Size)
   {
   ASSERT_SPI
   ASSERT_PARAMETER(u8Buffer == NULL)
   ASSERT_PARAMETER(u8Size > LCD_MAX_CHAR_SIZE)

   kLcd.kLineDc.GpioState = SET;
   HAL_GPIO_WritePin(kLcd.kLineDc.GpioPort, kLcd.kLineDc.GpioPin, kLcd.kLineDc.GpioState);
   kLcd.kLineCe.GpioState = RESET;
   HAL_GPIO_WritePin(kLcd.kLineCe.GpioPort, kLcd.kLineCe.GpioPin, kLcd.kLineCe.GpioState);

   HAL_SPI_Transmit(kLcd.hspi, u8Buffer, u8Size, 1000);
   HAL_Delay(1);
   kLcd.kLineCe.GpioState = SET;
   HAL_GPIO_WritePin(kLcd.kLineCe.GpioPort, kLcd.kLineCe.GpioPin, kLcd.kLineCe.GpioState);
   return HAL_OK;
   }

HAL_StatusTypeDef LcdClearLine(uint8_t u8Line)
   {
   ASSERT_SPI

   LcdSetPos(u8Line, 0);
   uint8_t u8Buffer[LCD_COLUMNS] =
      {
      0
      };

   kLcd.kLineDc.GpioState = SET;
   HAL_GPIO_WritePin(kLcd.kLineDc.GpioPort, kLcd.kLineDc.GpioPin, kLcd.kLineDc.GpioState);
   kLcd.kLineCe.GpioState = RESET;
   HAL_GPIO_WritePin(kLcd.kLineCe.GpioPort, kLcd.kLineCe.GpioPin, kLcd.kLineCe.GpioState);

   HAL_SPI_Transmit(kLcd.hspi, u8Buffer, LCD_COLUMNS, 1000);

   HAL_Delay(1);
   kLcd.kLineCe.GpioState = SET;
   HAL_GPIO_WritePin(kLcd.kLineCe.GpioPort, kLcd.kLineCe.GpioPin, kLcd.kLineCe.GpioState);
   return HAL_OK;
   }
HAL_StatusTypeDef LcdClearDisplay(void)
   {
   ASSERT_SPI
   for (uint8_t u8Line = 0; u8Line < LCD_ROWS; u8Line++)
      {
      LcdClearLine(u8Line);
      }
   return HAL_OK;
   }
