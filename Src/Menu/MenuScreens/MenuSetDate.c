/*
 * MenuSetDate.c
 *
 *  Created on: 6 kwi 2017
 *      Author: Kamil
 */

#include "../../../Inc/Menu/MenuScreens/MenuSetDate.h"
#include "../../../Bitmaps/images.h"

#define IMAGE_OFFSET (8)

typedef enum MenuOptions
   {
   eOption_Day,
   eOption_Month,
   eOption_Year,
   eOption_NumOf
   } MenuOption_T;

static uint32_t u32MyUwTick = 0;

static void SetupScreen(Date_T kLocalDate, MenuOption_T eMenuOption);
static void exitScreen(Menu_T *pkMenu, bool *pbIsFirstStart);
static void HandleButton_Up(MenuOption_T *peMenuOption);
static void HandleButton_Down(MenuOption_T *peMenuOption);
static void HandleButton_Left(MenuOption_T eMenuOption, Date_T *pkLocalDate);
static void HandleButton_Right(MenuOption_T eMenuOption, Date_T *pkLocalDate);
static void DisplayCheckBox_Marked(uint8_t posX, uint8_t posY);
static void DisplayCheckBox_Unmarked(uint8_t posX, uint8_t posY);

uint32_t CalculateElapsedTime(uint32_t u32MyUwTick);

Status_T Menu_SetDate_Perform(Aquarium_T *pkAquarium, Menu_T *pkMenu)
   {
   static Date_T kLocalDate;
   static MenuOption_T eMenuOption;
   static bool isFirstTime = true;

   if (isFirstTime)
      {
      u32MyUwTick = HAL_GetTick();
      eMenuOption = eOption_Day;
      isFirstTime = false;
      kLocalDate.u16Year = pkAquarium->MyDate.u16Year;
      kLocalDate.u8Month = pkAquarium->MyDate.u8Month;
      kLocalDate.u8Day = pkAquarium->MyDate.u8Day;
      }

   SetupScreen(kLocalDate, eMenuOption);

   if (pkAquarium->kParameters.ePressedButton == Button_Right)
      {
      HandleButton_Right(eMenuOption, &kLocalDate);
      }
   if (pkAquarium->kParameters.ePressedButton == Button_Left)
      {
      HandleButton_Left(eMenuOption, &kLocalDate);
      }
   if (pkAquarium->kParameters.ePressedButton == Button_Down)
      {
      HandleButton_Down(&eMenuOption);
      }
   if (pkAquarium->kParameters.ePressedButton == Button_Up)
      {
      HandleButton_Up(&eMenuOption);
      }

   if (pkAquarium->kParameters.ePressedButton == Button_Enter)
      {
      pkAquarium->MyDate.u16Year = kLocalDate.u16Year;
      pkAquarium->MyDate.u8Month = kLocalDate.u8Month;
      pkAquarium->MyDate.u8Day = kLocalDate.u8Day;
      exitScreen(pkMenu, &isFirstTime);
      }

   uint32_t u32ElapsedTime = CalculateElapsedTime(u32MyUwTick);

   if (pkAquarium->kParameters.ePressedButton == Button_Cancel || u32ElapsedTime >= 60000) // go back automatically after 1min
      {
      exitScreen(pkMenu, &isFirstTime);
      }
   return Status_Ok;
   }


static void SetupScreen(Date_T kLocalDate, MenuOption_T eMenuOption)
   {
   uint8_t u8Size = 0;
   char cTextBuffer[17];

   for (uint8_t index = eOption_Day; index < eOption_NumOf; ++index)
      {
      if (eMenuOption == index)
         {
         DisplayCheckBox_Marked(1, IMAGE_OFFSET + index * 8);
         }
      else
         {
         DisplayCheckBox_Unmarked(1, IMAGE_OFFSET + index * 8);
         }
      }

   u8Size = sprintf(cTextBuffer, "Day < %d%d >", kLocalDate.u8Day / 10, kLocalDate.u8Day % 10);
   LcdSetPos(1, 14);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   u8Size = sprintf(cTextBuffer, "Month < %d%d >", kLocalDate.u8Month / 10, kLocalDate.u8Month % 10);
   LcdSetPos(2, 14);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   u8Size = sprintf(cTextBuffer, "Year < %4d >", kLocalDate.u16Year);
   LcdSetPos(3, 14);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   u8Size = sprintf(cTextBuffer, "Press Enter");
   LcdSetPos(4, 1);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   u8Size = sprintf(cTextBuffer, "to accept");
   LcdSetPos(5, 1);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);
   }

static void DisplayCheckBox_Marked(uint8_t posX, uint8_t posY)
   {
   ImageStruct_T kImage = getImage(eCheckBoxMarked);
   LcdSetPos(posY / 8, posX);
   LcdDisplayBuffer(&(kImage.image[0]), kImage.size.width);
   }

static void DisplayCheckBox_Unmarked(uint8_t posX, uint8_t posY)
   {
   ImageStruct_T kImage = getImage(eCheckBoxUnmarked);
   LcdSetPos(posY / 8, posX);
   LcdDisplayBuffer(&(kImage.image[0]), kImage.size.width);
   }

static void exitScreen(Menu_T *pkMenu, bool *pbIsFirstStart)
   {
   (*pkMenu) = Menu_Settings;
   *pbIsFirstStart = true;
   }

static void HandleButton_Up(MenuOption_T *peMenuOption)
   {
   u32MyUwTick = HAL_GetTick();
   switch (*peMenuOption)
      {
      case eOption_Day:
         *peMenuOption = eOption_Year;
         break;
      case eOption_Month:
         *peMenuOption = eOption_Day;
         break;
      case eOption_Year:
         *peMenuOption = eOption_Year;
         break;
      case eOption_NumOf:
         break;
      }
   }

static void HandleButton_Down(MenuOption_T *peMenuOption)
   {
   u32MyUwTick = HAL_GetTick();
   switch (*peMenuOption)
      {
      case eOption_Day:
         *peMenuOption = eOption_Month;
         break;
      case eOption_Month:
         *peMenuOption = eOption_Year;
         break;
      case eOption_Year:
         *peMenuOption = eOption_Day;
         break;
      case eOption_NumOf:
         break;
      }
   }

static void HandleButton_Left(MenuOption_T eMenuOption, Date_T *pkLocalDate)
   {
   u32MyUwTick = HAL_GetTick();
   switch (eMenuOption)
      {
      case eOption_Day:
         pkLocalDate->u8Day--;
         break;
      case eOption_Month:
         pkLocalDate->u8Month--;
         break;
      case eOption_Year:
         pkLocalDate->u16Year--;
         break;
      case eOption_NumOf:
         break;
      }
   }

static void HandleButton_Right(MenuOption_T eMenuOption, Date_T *pkLocalDate)
   {
   u32MyUwTick = HAL_GetTick();
   switch (eMenuOption)
      {
      case eOption_Day:
         pkLocalDate->u8Day++;
         break;
      case eOption_Month:
         pkLocalDate->u8Month++;
         break;
      case eOption_Year:
         pkLocalDate->u16Year++;
         break;
      case eOption_NumOf:
         break;
      }
   }
