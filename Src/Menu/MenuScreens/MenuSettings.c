/*
 * MenuSettings.c
 *
 *  Created on: 6 kwi 2017
 *      Author: Kamil
 */

#include "../../../Inc/Menu/MenuScreens/MenuSettings.h"
#include "../../../Bitmaps/images.h"

#define IMAGE_OFFSET 16

static void DisplayCheckBox_Marked(uint8_t posX, uint8_t posY);
static void DisplayCheckBox_Unmarked(uint8_t posX, uint8_t posY);

static uint32_t u32MyUwTick = 0;

typedef enum MenuOptions
   {
   eOption_Date,
   eOption_Clock,
   eOption_WaterTemp,
   eOption_NumOf
   } MenuOption_T;

Status_T Menu_Settings_Perform(Aquarium_T *pkAquarium, Menu_T *pkMenu)
   {
   static bool isFirstTime = true;
   static MenuOption_T eOption;
   uint8_t u8Size = 0;
   char cTextBuffer[17];

   if (isFirstTime)
      {
      eOption = eOption_Date;
      u32MyUwTick = HAL_GetTick();
      isFirstTime = false;
      }

   u8Size = sprintf(cTextBuffer, "Settings");
   LcdSetPos(1, 1);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   for (uint8_t index = eOption_Date; index < eOption_NumOf; ++index)
      {
      if (eOption == index)
         {
         DisplayCheckBox_Marked(1, IMAGE_OFFSET + index * 8);
         }
      else
         {
         DisplayCheckBox_Unmarked(1, IMAGE_OFFSET + index * 8);
         }
      }

   u8Size = sprintf(cTextBuffer, "Date");
   LcdSetPos(2, 14);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   u8Size = sprintf(cTextBuffer, "Clock");
   LcdSetPos(3, 14);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   u8Size = sprintf(cTextBuffer, "Water Temp");
   LcdSetPos(4, 14);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   if (pkAquarium->kParameters.ePressedButton == Button_Down)
      {
      u32MyUwTick = HAL_GetTick();
      if (eOption == eOption_Date)
         eOption = eOption_Clock;
      else if (eOption == eOption_WaterTemp)
         eOption = eOption_Date;
      else if (eOption == eOption_Clock)
         eOption = eOption_WaterTemp;
      }
   if (pkAquarium->kParameters.ePressedButton == Button_Up)
      {
      u32MyUwTick = HAL_GetTick();
      if (eOption == eOption_Date)
         eOption = eOption_WaterTemp;
      else if (eOption == eOption_WaterTemp)
         eOption = eOption_Clock;
      else if (eOption == eOption_Clock)
         eOption = eOption_Date;
      }

   uint32_t u32ElapsedTime = CalculateElapsedTime(u32MyUwTick);
   if (pkAquarium->kParameters.ePressedButton == Button_Cancel || u32ElapsedTime >= 60000)
      {
      (*pkMenu) = Menu_Main;
      isFirstTime = true;
      }

   if (pkAquarium->kParameters.ePressedButton == Button_Enter)
      {
      switch (eOption)
         {
         case eOption_Clock:
            (*pkMenu) = Menu_SetClock;
            break;
         case eOption_Date:
            (*pkMenu) = Menu_SetDate;
            break;
         case eOption_WaterTemp:
            (*pkMenu) = Menu_Temp;
            break;
         default:
            break;
         }
      isFirstTime = true;
      }

   return Status_Ok;
   }

static void DisplayCheckBox_Marked(uint8_t posX, uint8_t posY)
   {
   ImageStruct_T kImage = getImage(eCheckBoxMarked);
   LcdSetPos(posY / 8, posX);
   LcdDisplayBuffer(&(kImage.image[0]), kImage.size.width);
   }

static void DisplayCheckBox_Unmarked(uint8_t posX, uint8_t posY)
   {
   ImageStruct_T kImage = getImage(eCheckBoxUnmarked);
   LcdSetPos(posY / 8, posX);
   LcdDisplayBuffer(&(kImage.image[0]), kImage.size.width);
   }
