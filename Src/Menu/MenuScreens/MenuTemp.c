/*
 * MenuTemp.c
 *
 *  Created on: 6 kwi 2017
 *      Author: Kamil
 */

#include "../../../Inc/Menu/MenuScreens/MenuTemp.h"
#include "../../../Bitmaps/images.h"

#define IMAGE_OFFSET 24

static void DisplayCheckBox_Marked(uint8_t posX, uint8_t posY);
static void DisplayCheckBox_Unmarked(uint8_t posX, uint8_t posY);

typedef enum MenuOptions
   {
   eOption_Day,
   eOption_Night,
   eOption_NumOf
   } MenuOption_T;

   static uint32_t u32MyUwTick = 0;

Status_T Menu_Temp_Perform(Aquarium_T *pkAquarium, Menu_T *pkMenu)
   {
   static bool isFirstTime = true;
   static MenuOption_T eOption;
   uint8_t u8Size = 0;
   char cTextBuffer[17];

   if (isFirstTime)
      {
      eOption = eOption_Day;
      u32MyUwTick = HAL_GetTick();
      isFirstTime = false;
      }

   u8Size = sprintf(cTextBuffer, "Water Temp");
   LcdSetPos(1, 1);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   for (uint8_t index = eOption_Day; index < eOption_NumOf; ++index)
      {
      if (eOption == index)
         {
         DisplayCheckBox_Marked(1, IMAGE_OFFSET + index * 8);
         }
      else
         {
         DisplayCheckBox_Unmarked(1, IMAGE_OFFSET + index * 8);
         }
      }
   u8Size = sprintf(cTextBuffer, "Day");
   LcdSetPos(3, 14);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   u8Size = sprintf(cTextBuffer, "Night");
   LcdSetPos(4, 14);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   if (pkAquarium->kParameters.ePressedButton == Button_Down)
      {
      u32MyUwTick = HAL_GetTick();
      if (eOption == eOption_Night)
         eOption = eOption_Day;
      else if (eOption == eOption_Day)
         eOption = eOption_Night;
      }
   if (pkAquarium->kParameters.ePressedButton == Button_Up)
      {
      u32MyUwTick = HAL_GetTick();
      if (eOption == eOption_Day)
         eOption = eOption_Night;
      else if (eOption == eOption_Night)
         eOption = eOption_Day;
      }

   uint32_t u32ElapsedTime = CalculateElapsedTime(u32MyUwTick);

   if (pkAquarium->kParameters.ePressedButton == Button_Cancel) //|| u32ElapsedTime >= 60000) // uncomment it if you want to go back automatically after 1min
      {
      (*pkMenu) = Menu_Settings;
      isFirstTime = true;
      }

   if (pkAquarium->kParameters.ePressedButton == Button_Enter)
      {
      switch (eOption)
         {
         case eOption_Day:
            (*pkMenu) = Menu_SetDayTemp;
            break;
         case eOption_Night:
            (*pkMenu) = Menu_SetNightTemp;
            break;
         default:
            break;
         }
      isFirstTime = true;
      }
   return Status_Ok;
   }

static void DisplayCheckBox_Marked(uint8_t posX, uint8_t posY)
   {
   ImageStruct_T kImage = getImage(eCheckBoxMarked);
   LcdSetPos(posY / 8, posX);
   LcdDisplayBuffer(&(kImage.image[0]), kImage.size.width);
   }

static void DisplayCheckBox_Unmarked(uint8_t posX, uint8_t posY)
   {
   ImageStruct_T kImage = getImage(eCheckBoxUnmarked);
   LcdSetPos(posY / 8, posX);
   LcdDisplayBuffer(&(kImage.image[0]), kImage.size.width);
   }

