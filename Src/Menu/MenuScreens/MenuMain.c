/*
 * MenuMain.c
 *
 *  Created on: 6 kwi 2017
 *      Author: Kamil
 */

#include "../../../Inc/Menu/MenuScreens/MenuMain.h"
#include "../../../Bitmaps/images.h"

void DisplayFish(uint8_t posX, uint8_t posY);
void DisplayHeater(uint8_t posX, uint8_t posY);
void DisplayFan(uint8_t posX, uint8_t posY);

Status_T Menu_Main_Perform(Aquarium_T *pkAquarium, Menu_T *pkMenu)
   {
   char cTextBuffer[17];

   uint8_t u8Size = sprintf(cTextBuffer, "Day");
   LcdSetPos(1, 1);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   DisplayFish(10, 16);
   DisplayFish(45, 16);

   if (pkAquarium->kParameters.eHeater == Device_On)
      DisplayHeater(11, 32);
   if (pkAquarium->kParameters.eFans == Device_On)
      {
      DisplayFan(51, 32);
      DisplayFan(59, 32);
      DisplayFan(67, 32);
      }
   if (pkAquarium->kParameters.eOperatingMode == eMode_Day)
      u8Size = sprintf(cTextBuffer, "%2.1f", pkAquarium->kParameters.fDayTempToKeep);
   else if (pkAquarium->kParameters.eOperatingMode == eMode_Night)
      u8Size = sprintf(cTextBuffer, "%2.1f", pkAquarium->kParameters.fNightTempToKeep);
   LcdSetPos(1, 60);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   u8Size = sprintf(cTextBuffer, "%2.1f", pkAquarium->kParameters.fWaterTemp);
   LcdSetPos(5, 1);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   u8Size = sprintf(cTextBuffer, "%2.1f", pkAquarium->kParameters.fAmbientTemp);
   LcdSetPos(5, 60);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   if (pkAquarium->kParameters.ePressedButton == Button_Menu)
      {
      (*pkMenu) = Menu_Settings;
      }
   return Status_Ok;
   } //End of MainMenu perform

/*
 *
 */
void DisplayFish(uint8_t posX, uint8_t posY)
   {
   ImageStruct_T kImage = getImage(eFish);
   LcdSetPos(posY / 8, posX);
   LcdDisplayBuffer(&(kImage.image[0]), kImage.size.width);
   LcdSetPos((posY / 8) + 1, posX);
   LcdDisplayBuffer(&(kImage.image[kImage.size.width]), kImage.size.width);
   }

void DisplayHeater(uint8_t posX, uint8_t posY)
   {
   ImageStruct_T kImage = getImage(eCoilHeater);
   LcdSetPos(posY / 8, posX);
   LcdDisplayBuffer(&(kImage.image[0]), kImage.size.width);
   }

void DisplayFan(uint8_t posX, uint8_t posY)
   {
   ImageStruct_T kImage = getImage(eFan);
   LcdSetPos(posY / 8, posX);
   LcdDisplayBuffer(&(kImage.image[0]), kImage.size.width);
   }

