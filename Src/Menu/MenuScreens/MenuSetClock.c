/*
 * MenuSetClock.c
 *
 *  Created on: 6 kwi 2017
 *      Author: Kamil
 */

#include "../../../Inc/Menu/MenuScreens/MenuSetClock.h"
#include "../../../Bitmaps/images.h"

#define IMAGE_OFFSET 8

typedef enum MenuOptions
   {
   eOption_Hour,
   eOption_Minute,
   eOption_NumOf
   } MenuOption_T;

static uint32_t u32MyUwTick = 0;

static void SetupScreen(Date_T kLocalDate, MenuOption_T eMenuOption);
static void exitScreen(Menu_T *pkMenu, bool *pbIsFirstStart);
static void HandleButton_Up(MenuOption_T *peMenuOption);
static void HandleButton_Down(MenuOption_T *peMenuOption);
static void HandleButton_Left(MenuOption_T eMenuOption, Date_T *pkLocalDate);
static void HandleButton_Right(MenuOption_T eMenuOption, Date_T *pkLocalDate);
static void DisplayCheckBox_Marked(uint8_t posX, uint8_t posY);
static void DisplayCheckBox_Unmarked(uint8_t posX, uint8_t posY);

Status_T Menu_SetClock_Perform(Aquarium_T *pkAquarium, Menu_T *pkMenu)
   {
   static Date_T kLocalTime;
   static MenuOption_T eMenuOption = eOption_Hour;
   static bool isFirstTime = true;

   if (isFirstTime)
      {
      u32MyUwTick = HAL_GetTick();
      eMenuOption = eOption_Hour;
      isFirstTime = false;
      kLocalTime.u8Hour = pkAquarium->MyDate.u8Hour;
      kLocalTime.u8Minutes = pkAquarium->MyDate.u8Minutes;
      }

   SetupScreen(kLocalTime, eMenuOption);

   if (pkAquarium->kParameters.ePressedButton == Button_Right)
      {
      HandleButton_Right(eMenuOption, &kLocalTime);
      }
   if (pkAquarium->kParameters.ePressedButton == Button_Left)
      {
      HandleButton_Left(eMenuOption, &kLocalTime);
      }

   if (pkAquarium->kParameters.ePressedButton == Button_Up)
      {
      HandleButton_Up(&eMenuOption);
      }
   if (pkAquarium->kParameters.ePressedButton == Button_Down)
      {
      HandleButton_Down(&eMenuOption);
      }

   if (pkAquarium->kParameters.ePressedButton == Button_Enter)
      {
      pkAquarium->MyDate.u8Hour = kLocalTime.u8Hour;
      pkAquarium->MyDate.u8Minutes = kLocalTime.u8Minutes;
      (*pkMenu) = Menu_Settings;
      isFirstTime = true;
      }
   uint32_t u32ElapsedTime = CalculateElapsedTime(u32MyUwTick);
   if (pkAquarium->kParameters.ePressedButton == Button_Cancel || u32ElapsedTime >= 60000) // go back automatically after 1min
      {
      exitScreen(pkMenu, &isFirstTime);
      }
   return Status_Ok;
   }

static void SetupScreen(Date_T kLocalDate, MenuOption_T eMenuOption)
   {
   uint8_t u8Size = 0;
   char cTextBuffer[17];

   for (uint8_t index = eOption_Hour; index < eOption_NumOf; ++index)
      {
      if (eMenuOption == index)
         {
         DisplayCheckBox_Marked(1, IMAGE_OFFSET + index * 8);
         }
      else
         {
         DisplayCheckBox_Unmarked(1, IMAGE_OFFSET + index * 8);
         }
      }
   u8Size = sprintf(cTextBuffer, "Hour < %d%d >", kLocalDate.u8Hour / 10, kLocalDate.u8Hour % 10);
   LcdSetPos(1, 14);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   u8Size = sprintf(cTextBuffer, "Minute< %d%d >", kLocalDate.u8Minutes / 10, kLocalDate.u8Minutes % 10);
   LcdSetPos(2, 14);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   u8Size = sprintf(cTextBuffer, "Press Enter");
   LcdSetPos(4, 1);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   u8Size = sprintf(cTextBuffer, "to accept");
   LcdSetPos(5, 1);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);
   }

static void DisplayCheckBox_Marked(uint8_t posX, uint8_t posY)
   {
   ImageStruct_T kImage = getImage(eCheckBoxMarked);
   LcdSetPos(posY / 8, posX);
   LcdDisplayBuffer(&(kImage.image[0]), kImage.size.width);
   }

static void DisplayCheckBox_Unmarked(uint8_t posX, uint8_t posY)
   {
   ImageStruct_T kImage = getImage(eCheckBoxUnmarked);
   LcdSetPos(posY / 8, posX);
   LcdDisplayBuffer(&(kImage.image[0]), kImage.size.width);
   }

static void exitScreen(Menu_T *pkMenu, bool *pbIsFirstStart)
   {
   (*pkMenu) = Menu_Settings;
   *pbIsFirstStart = true;
   }

static void HandleButton_Up(MenuOption_T *peMenuOption)
   {
   u32MyUwTick = HAL_GetTick();
   switch (*peMenuOption)
      {
      case eOption_Minute:
         *peMenuOption = eOption_Hour;
         break;
      case eOption_Hour:
         *peMenuOption = eOption_Minute;
         break;
      default:
         break;
      }
   }

static void HandleButton_Down(MenuOption_T *peMenuOption)
   {
   u32MyUwTick = HAL_GetTick();
   switch (*peMenuOption)
      {
      case eOption_Hour:
         *peMenuOption = eOption_Minute;
         break;
      case eOption_Minute:
         *peMenuOption = eOption_Hour;
         break;
      default:
         break;
      }
   }

static void HandleButton_Left(MenuOption_T eMenuOption, Date_T *pkLocalDate)
   {
   u32MyUwTick = HAL_GetTick();
   switch (eMenuOption)
      {
      case eOption_Hour:
         pkLocalDate->u8Hour--;
         break;
      case eOption_Minute:
         pkLocalDate->u8Minutes--;
         break;
      default:
         break;
      }
   }

static void HandleButton_Right(MenuOption_T eMenuOption, Date_T *pkLocalDate)
   {
   u32MyUwTick = HAL_GetTick();
   switch (eMenuOption)
      {
      case eOption_Hour:
         pkLocalDate->u8Hour++;
         break;
      case eOption_Minute:
         pkLocalDate->u8Minutes++;
         break;
      default:
         break;
      }
   }
