/*
 * MenuSetDayTemp.c
 *
 *  Created on: 6 kwi 2017
 *      Author: Kamil
 */

#include "../../../Inc/Menu/MenuScreens/MenuSetDayTemp.h"


static uint32_t u32MyUwTick = 0;

Status_T Menu_SetDayTemp_Perform(Aquarium_T *pkAquarium, Menu_T *pkMenu)
   {
   static bool isFirstTime = true;
   uint8_t u8Size = 0;
   char cTextBuffer[17];
   static float fLocalTemp = 0;

   if (isFirstTime)
      {
      fLocalTemp = pkAquarium->kParameters.fDayTempToKeep;
      u32MyUwTick = HAL_GetTick();
      isFirstTime = false;
      }

   u8Size = sprintf(cTextBuffer, "Water Temp");
   LcdSetPos(1, 1);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   u8Size = sprintf(cTextBuffer, "Day");
   LcdSetPos(2, 1);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   u8Size = sprintf(cTextBuffer, "< %2.1f >", fLocalTemp);
   LcdSetPos(3, 1);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   u8Size = sprintf(cTextBuffer, "Press Enter");
   LcdSetPos(4, 1);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   u8Size = sprintf(cTextBuffer, "to accept");
   LcdSetPos(5, 1);
   LcdDisplayText((uint8_t *) cTextBuffer, u8Size);

   if (pkAquarium->kParameters.ePressedButton == Button_Right)
      {
      fLocalTemp += 0.1;
      u32MyUwTick = HAL_GetTick();
      }

   if (pkAquarium->kParameters.ePressedButton == Button_Left)
      {
      fLocalTemp -= 0.1;
      u32MyUwTick = HAL_GetTick();
      }

   if (pkAquarium->kParameters.ePressedButton == Button_Enter)
      {
      (*pkMenu) = Menu_Temp;
      pkAquarium->kParameters.fDayTempToKeep = fLocalTemp;
      isFirstTime = true;
      }
   uint32_t u32ElapsedTime = CalculateElapsedTime(u32MyUwTick);
   if (pkAquarium->kParameters.ePressedButton == Button_Cancel) //|| u32ElapsedTime >= 60000) // uncomment it if you want to go back automatically after 1min
      {
      (*pkMenu) = Menu_Temp;
      isFirstTime = true;
      }
   return Status_Ok;
   }


