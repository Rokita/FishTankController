/*
 * Menu.c
 *
 *  Created on: 6 gru 2016
 *      Author: Kamil
 */

#include "../../Inc/Menu/Menu.h"
#include "../../Inc/Menu/MenuScreens/MenuMain.h"
#include "../../Inc/Menu/MenuScreens/MenuSetClock.h"
#include "../../Inc/Menu/MenuScreens/MenuSetDate.h"
#include "../../Inc/Menu/MenuScreens/MenuSetDayTemp.h"
#include "../../Inc/Menu/MenuScreens/MenuSetNightTemp.h"
#include "../../Inc/Menu/MenuScreens/MenuSettings.h"
#include "../../Inc/Menu/MenuScreens/MenuTemp.h"


void DisplayDate(Date_T MyDate);
//////////////////////////////////////
Status_T Menu_Perform(Aquarium_T *pkAquarium, Menu_T *pkMenu)
//////////////////////////////////////
   {
   Status_T eReturnStatus = Status_Error;
   LcdClearDisplay();
   DisplayDate(pkAquarium->MyDate);

   switch (*pkMenu)
      {
      case Menu_Main:
         eReturnStatus = Menu_Main_Perform(pkAquarium, pkMenu);
         break;
      case Menu_Settings:
         eReturnStatus = Menu_Settings_Perform(pkAquarium, pkMenu);
         break;
      case Menu_Temp:
         eReturnStatus = Menu_Temp_Perform(pkAquarium, pkMenu);
         break;
      case Menu_SetDate:
         eReturnStatus = Menu_SetDate_Perform(pkAquarium, pkMenu);
         break;
      case Menu_SetClock:
         eReturnStatus = Menu_SetClock_Perform(pkAquarium, pkMenu);
         break;
      case Menu_SetNightTemp:
         eReturnStatus = Menu_SetNightTemp_Perform(pkAquarium, pkMenu);
         break;
      case Menu_SetDayTemp:
         eReturnStatus = Menu_SetDayTemp_Perform(pkAquarium, pkMenu);
         break;
      default:  //This has to be here in case of wrong value in menu
         break;
      }
   return eReturnStatus;
   }

void DisplayDate(Date_T MyNewDate)
   {
   char cTextBuffer[12];
   uint8_t u8Size;

   LcdSetPos(0, 1);
   u8Size = sprintf(cTextBuffer, "%d%d.%d%d.%4d", MyNewDate.u8Day / 10, MyNewDate.u8Day % 10, MyNewDate.u8Month / 10,
         MyNewDate.u8Month % 10, MyNewDate.u16Year);
   if (u8Size > 0)
      {
      LcdDisplayText((uint8_t*) cTextBuffer, u8Size);
      }
   LcdSetPos(0, 58);
   u8Size = sprintf(cTextBuffer, "%d%d:%d%d", MyNewDate.u8Hour / 10, MyNewDate.u8Hour % 10, MyNewDate.u8Minutes / 10,
         MyNewDate.u8Minutes % 10);
   if (u8Size > 0)
      {
      LcdDisplayText((uint8_t*) cTextBuffer, u8Size);
      }
   }

