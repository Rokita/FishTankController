/*
 * Board.c
 *
 *  Created on: 6 gru 2016
 *      Author: Kamil
 */

#include "../../Inc/Board/Board.h"

static Button_T previousButton = Button_None;
//////////////////////////////////////
Button_T GetPressedButton(void)
//////////////////////////////////////
   {
   ADC_ChannelConfTypeDef sConfig;
   uint16_t u16AdcMeasurement;   // 12-bit ADC measurement value (0 - 4095)

   sConfig.Channel = BUTTON_ADC_CHANNEL;
   sConfig.Rank = 1;
   sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
   if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
      {
      Error_Handler();
      }
   HAL_ADC_Start(&hadc1);

   Button_T actualButton = Button_Error;
   if (HAL_ADC_PollForConversion(&hadc1, 100) == HAL_OK)
      {
      u16AdcMeasurement = HAL_ADC_GetValue(&hadc1) + (BUTTON_STEP_VALUE / 2);                        //get ADC value

      actualButton = (Button_T) (u16AdcMeasurement / BUTTON_STEP_VALUE);

      if (actualButton != previousButton)
         previousButton = actualButton;
      else
         actualButton = Button_None;
      }
   return actualButton;
   }

//////////////////////////////////////
float GetWaterTemperature(void)
//////////////////////////////////////
   {
   ADC_ChannelConfTypeDef sConfig;
   uint16_t u16AdcMeasurement = 0;   // 12-bit ADC measurement value (0 - 4095)
   float fVoltage = 0;
   float fTemperature = 0;

   sConfig.Channel = WATER_TEMP_CHANNEL;
   sConfig.Rank = 1;
   sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
   if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
      {
      Error_Handler();
      }
   HAL_ADC_Start(&hadc1);
   if (HAL_ADC_PollForConversion(&hadc1, 100) == HAL_OK)
      {
      u16AdcMeasurement = HAL_ADC_GetValue(&hadc1);
      fVoltage = ((float) u16AdcMeasurement) * POWER_SUPLY / ADC_RESOLUTION;
      fTemperature = (fVoltage - MCP9700_TEMP_0_VOLTAGE) / MCP9700_TEMP_COEFFICIENT;
      }
   return fTemperature;
   }

//////////////////////////////////////
float GetAmbientTemperature(void)
//////////////////////////////////////
   {
   ADC_ChannelConfTypeDef sConfig;
   uint16_t u16AdcMeasurement = 0;   // 12-bit ADC measurement value (0 - 4095)
   float fVoltage = 0;
   float fTemperature = 0;

   sConfig.Channel = AMBIENT_TEMP_CHANNEL;
   sConfig.Rank = 1;
   sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
   if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
      {
      Error_Handler();
      }
   HAL_ADC_Start(&hadc1);
   if (HAL_ADC_PollForConversion(&hadc1, 100) == HAL_OK)
      {
      u16AdcMeasurement = HAL_ADC_GetValue(&hadc1);
      fVoltage = ((float) u16AdcMeasurement) * POWER_SUPLY / ADC_RESOLUTION;
      fTemperature = (fVoltage - MCP9700_TEMP_0_VOLTAGE) / MCP9700_TEMP_COEFFICIENT;
      }
   return fTemperature;
   }

//////////////////////////////////////
Status_T SetLed(DeviceState_T eLed1, DeviceState_T eLed2, DeviceState_T eLed3)
//////////////////////////////////////
   {
   HAL_GPIO_WritePin(Led1_GPIO_Port, Led1_Pin, eLed1);
   HAL_GPIO_WritePin(Led2_GPIO_Port, Led2_Pin, eLed2);
   HAL_GPIO_WritePin(Led3_GPIO_Port, Led3_Pin, eLed3);
   return Status_Ok;
   }

//////////////////////////////////////
Status_T SetFan(DeviceState_T eFan1)
//////////////////////////////////////
   {
   HAL_GPIO_WritePin(DcFans_GPIO_Port, DcFans_Pin, !eFan1);
   return Status_Ok;
   }

//////////////////////////////////////
Status_T SetCoilHeater(DeviceState_T eCoilHeater)
//////////////////////////////////////
   {
   HAL_GPIO_WritePin(CoilHeater_GPIO_Port, CoilHeater_Pin, eCoilHeater);
   return Status_Ok;
   }

//////////////////////////////////////
Status_T SetFilter(DeviceState_T eFilter)
//////////////////////////////////////
   {
   HAL_GPIO_WritePin(Filter_GPIO_Port, Filter_Pin, eFilter);
   return Status_Ok;
   }

//////////////////////////////////////
Status_T SetPowerLedInPercent(uint8_t u8PwmInPercent)
//////////////////////////////////////
   {
   static uint8_t u8PreviousPwmInPercent = 0;
   if (u8PreviousPwmInPercent == u8PwmInPercent)
      {
      return Status_Ok;
      }
   if (u8PwmInPercent > 100)
      {
      u8PwmInPercent = 100;
      }
   htim3.Init.Period = 99;
   TIM_OC_InitTypeDef sConfigOC;

   sConfigOC.OCMode = TIM_OCMODE_PWM1;
   sConfigOC.Pulse = ((((float) u8PwmInPercent) / 100) * htim3.Init.Period);
   sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
   sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
   if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
      {
      Error_Handler();
      }
   HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
   return Status_Ok;
   }

